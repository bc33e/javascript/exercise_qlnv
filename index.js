var danhSachNv = [];
const LIST_NV = "danhSachNhanVien";

// lấy danh sách từ storage dưới dạng json
let listNhanVienLocalStorage = localStorage.getItem(LIST_NV);


//add lại dữ liệu vào bảng
if (JSON.parse(listNhanVienLocalStorage)) {
  var dataNhanVien = JSON.parse(listNhanVienLocalStorage);
  console.log("dataNhanVien before: ", dataNhanVien);
  for (var i = 0; i < dataNhanVien.length; i++) {
    var current = dataNhanVien[i];
    var nv = new nhanVien(
      current.taiKhoan,
      current.tenNv,
      current.email,
      current.password,
      current.ngayLam,
      current.luongCb,
      current.chucVu,
      current.gioLam
    );
    danhSachNv.push(nv);
  }
  renderListNhanVien(danhSachNv);
}

function saveData() {
  //convert array thành json
  var danhSachNhanVienJson = JSON.stringify(danhSachNv);
  console.log("danhSachNhanVienJson: ", danhSachNhanVienJson);
  // lưu xuống local storage
  localStorage.setItem(LIST_NV, danhSachNhanVienJson);
}

// ----- HÀM THEM NHÂN VIÊN ------
function btnThemNv() {
  var newNhanVien = getInfoFromForm();
  console.log("newNhanVien: ", newNhanVien);

  var isValid =
    validator.kiemTraChuoiRong(
      newNhanVien.taiKhoan,
      "tbTKNV", "Tài khoản không được để trống"
    ) &&
    validator.kiemTraTaiKhoan(newNhanVien.taiKhoan, danhSachNv) &&
    validator.kiemTraDoDai(newNhanVien.taiKhoan, "tbTKNV", 4, 6) &&
    validator.kiemTraChuoiSo(newNhanVien.taiKhoan, "tbTKNV");

  //kiểm tra tên
  isValid =
    isValid &
      validator.kiemTraChuoiRong(
        newNhanVien.tenNv,
        "tbTen",
        "Họ tên không được để trống"
      ) && validator.kiemTraChuoiChu(newNhanVien.tenNv, "tbTen");

  //kiểm tra email
  isValid =
    isValid &
      validator.kiemTraChuoiRong(
        newNhanVien.email,
        "tbEmail",
        "Email không được để trống"
      ) && validator.kiemTraEmail(newNhanVien.email, "tbEmail");

  // kiểm tra mật khẩu
  isValid =
    isValid &
      validator.kiemTraChuoiRong(
        newNhanVien.password,
        "tbMatKhau",
        "Mật khẩu không được để trống"
      ) &&
    validator.kiemTraDoDai(newNhanVien.password, "tbMatKhau", 6, 10) &&
    validator.kiemTraMatKhau(newNhanVien.password, "tbMatKhau");

  // kiểm tra ngày làm
  isValid =
    isValid &
      validator.kiemTraChuoiRong(
        newNhanVien.ngayLam,
        "tbNgay",
        "Ngày làm không được để trống"
      ) && validator.kiemTraNgay(newNhanVien.ngayLam, "tbNgay");

  // kiểm tra lương cơ bản
  isValid =
    isValid &
      validator.kiemTraChuoiRong(
        newNhanVien.luongCb,
        "tbLuongCB",
        "Lương cơ bản không được để trống"
      ) &&
    validator.kiemTraChuoiSo(newNhanVien.luongCb, "tbLuongCB") &&
    validator.kiemTraGiaTri(newNhanVien.luongCb, "tbLuongCB", 1000000, 20000000);

  // kiểm tra chức vụ
  isValid =
    isValid &
    validator.kiemTraChucVu(
      newNhanVien.chucVu,
      "tbChucVu",
    );

  // kiểm tra giờ làm
  isValid =
    isValid &
      validator.kiemTraChuoiRong(
        newNhanVien.gioLam,
        "tbGiolam",
        "Giờ làm không được để trống"
      ) &&
    validator.kiemTraChuoiSo(newNhanVien.gioLam, "tbGiolam") &&
    validator.kiemTraGiaTri(newNhanVien.gioLam, "tbGiolam", 80, 200);

  if (isValid == false) {
    return;
  }

  danhSachNv.push(newNhanVien);
  saveData();
  renderListNhanVien(danhSachNv);
  
  document.getElementById("myform").reset()
  location.reload()
}

// hàm sửa nhân viên
function suaNv(id){
  document.getElementById("tknv").disabled = true;
  var index = timKiemViTri(id, danhSachNv);
  if (index !== -1) {
    hienThiThongTin(danhSachNv[index]);
  }
}

// hàm cập nhật nhân viên
function btnCapNhatNv(){
  var capNhatNv = getInfoFromForm();
  console.log("capNhatNv: ", capNhatNv);
  let index = timKiemViTri(capNhatNv.taiKhoan, danhSachNv);
  if (index !== -1) {
    danhSachNv[index] = capNhatNv;

    var isValid = validator.kiemTraChuoiRong(
      capNhatNv.tenNv,
      "tbTen",
      "Họ tên không được để trống"
    ) && validator.kiemTraChuoiChu(capNhatNv.tenNv, "tbTen");
  
     //kiểm tra email
     isValid =
     isValid &
       validator.kiemTraChuoiRong(
        capNhatNv.email,
         "tbEmail",
         "Email không được để trống"
       ) && validator.kiemTraEmail(capNhatNv.email, "tbEmail");
  
   // kiểm tra mật khẩu
   isValid =
     isValid &
       validator.kiemTraChuoiRong(
        capNhatNv.password,
         "tbMatKhau",
         "Mật khẩu không được để trống"
       ) &&
     validator.kiemTraDoDai(capNhatNv.password, "tbMatKhau", 6, 10) &&
     validator.kiemTraMatKhau(capNhatNv.password, "tbMatKhau");
  
   // kiểm tra ngày làm
   isValid =
     isValid &
       validator.kiemTraChuoiRong(
        capNhatNv.ngayLam,
         "tbNgay",
         "Ngày làm không được để trống"
       ) && validator.kiemTraNgay(capNhatNv.ngayLam, "tbNgay");
  
   // kiểm tra lương cơ bản
   isValid =
     isValid &
       validator.kiemTraChuoiRong(
        capNhatNv.luongCb,
         "tbLuongCB",
         "Lương cơ bản không được để trống"
       ) &&
     validator.kiemTraChuoiSo(capNhatNv.luongCb, "tbLuongCB") &&
     validator.kiemTraGiaTri(capNhatNv.luongCb, "tbLuongCB", 1000000, 20000000);
  
   // kiểm tra chức vụ
   isValid =
     isValid &
     validator.kiemTraChucVu(
      capNhatNv.chucVu,
       "tbChucVu",
     );
  
   // kiểm tra giờ làm
   isValid =
     isValid &
       validator.kiemTraChuoiRong(
        capNhatNv.gioLam,
         "tbGiolam",
         "Giờ làm không được để trống"
       ) &&
     validator.kiemTraChuoiSo(capNhatNv.gioLam, "tbGiolam") &&
     validator.kiemTraGiaTri(capNhatNv.gioLam, "tbGiolam", 80, 200);
  
   if (isValid == false) {
     return;
   }

    saveData();
    renderListNhanVien(danhSachNv);
    alert("Cập nhật nhân viên thành công");
    document.getElementById("myform").reset()
  }

  
}

// hàm xóa nhân viên
function xoaNv(id) {
  // console.log("Xóa sv: ", id);
  var index = timKiemViTri(id, danhSachNv);
  console.log("Vị trí xóa: ", i);
  if (index !== -1) {
    danhSachNv.splice(index, 1);
    // console.log("danhsachSv after: ", danhsachSv);
    saveData();
    renderListNhanVien(danhSachNv);
    location.reload()
  
  }
}

// hàm tìm kiếm 
document.getElementById("btnTimNV").onclick = function(){
  var searchName = document.getElementById("searchName").value;
  var searchList = [];
 danhSachNv.forEach((nVien) => {
  if(searchName == nVien.xepLoaiNv()){
    searchList.push(nVien);
    renderListNhanVien(searchList)
  }
  if(searchName ==""){
    renderListNhanVien(danhSachNv)
  }
 })
 

}





