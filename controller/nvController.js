function getInfoFromForm(){
    var taiKhoan =  document.getElementById("tknv").value.trim();
    var tenNv =  document.getElementById("txt-name").value.trim();
    var email =  document.getElementById("email").value.trim();
    var password =  document.getElementById("password").value;
    var ngayLam =  document.getElementById("datepicker").value.trim();
    var luongCb =  document.getElementById("luongCB").value.trim();
    var chucVu =  document.getElementById("chucvu").value;
    var gioLam =  document.getElementById("gioLam").value.trim();

    var nv = new nhanVien(taiKhoan, tenNv, email, password, ngayLam, luongCb, chucVu, gioLam)
    // console.log("Nhân viên ", nv.tongLuong())
    return nv;
}

function renderListNhanVien(list){
    var contentHtml = "";
    list.forEach(function(item){
        var content = `
        <tr>
        <td>${item.taiKhoan}</td>
        <td>${item.tenNv}</td>
        <td>${item.email}</td>
        <td>${item.ngayLam}</td>
        <td>${item.chucVu}</td>
        <td>${item.tongLuong()} VND</td>
        <td>${item.xepLoaiNv()}</td>
        <td>
        <button class="btn btn-info" data-toggle="modal" data-target="#myModal" onclick="suaNv(${item.taiKhoan})">Sửa</button>
        <button class="btn btn-danger" onclick="xoaNv(${item.taiKhoan})">Xóa</button>
        </td>
        </tr>
        `;
        contentHtml += content;
        document.getElementById("tableDanhSach").innerHTML = contentHtml;
    })
}

function timKiemViTri(id, arr) {
    return arr.findIndex(function(nv){
      return nv.taiKhoan == id;
    })
}

function hienThiThongTin(nv){
    document.getElementById("tknv").value = nv.taiKhoan;
    document.getElementById("txt-name").value = nv.tenNv;
    document.getElementById("email").value = nv.email;
    document.getElementById("password").value = nv.password;
    document.getElementById("datepicker").value = nv.ngayLam;
    document.getElementById("luongCB").value = nv.luongCb;
    document.getElementById("chucvu").value = nv.chucVu;
    document.getElementById("gioLam").value = nv.gioLam;
}