function nhanVien(taiKhoan, tenNv, email, password, ngayLam, luongCb, chucVu, gioLam){
    this.taiKhoan = taiKhoan;
    this.tenNv = tenNv;
    this.email = email;
    this.password = password;
    this.ngayLam = ngayLam;
    this.luongCb = luongCb;
    this.chucVu = chucVu;
    this.gioLam = gioLam;
    this.tongLuong = function(){
        if(chucVu == "Sếp"){
            return this.luongCb*3;
        }
        if(chucVu == "Trưởng phòng"){
            return this.luongCb*2;
        } 
        if(chucVu == "Nhân viên"){
            return this.luongCb;
        } 
    };

    this.xepLoaiNv = function(){
        var xepLoai ="";
        if (this.gioLam >= 192) {
            xepLoai = "Xuất sắc";
          } else if (this.gioLam >= 176 && this.gioLam < 192) {
            xepLoai = "Giỏi";
          } else if (this.gioLam >= 160 && this.gioLam < 176) {
            xepLoai = "Khá";
          } else {
            xepLoai = "Trung bình";
          }
          return xepLoai;
    }
}